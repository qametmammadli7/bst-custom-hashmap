package com.qamet.hashmap;

import java.util.Objects;

public class MyHashMap<K, V> {
    public final int INITIAL_SIZE = 16;
    int size = 0;
    public Node<K, V>[] buckets;

    public static void main(String[] args) {
        System.out.println("Hello world!");

        MyHashMap<Object, Object> map = new MyHashMap<>();
        map.put("key1", 1);
        map.put("key1", "updated");
        System.out.println("size : " + map.getSize());
        map.put("key2", 2);
        map.display();
        System.out.println("size : " + map.getSize());

        System.out.println("key1 value : " + map.get("key1"));

        map.remove("key2");

        System.out.println("size : " + map.getSize());

        map.put("key3", "new 3");
        map.put("key4", "new 3");
        System.out.println("size : " + map.getSize());

    }

    public MyHashMap() {
        this.buckets = new Node[INITIAL_SIZE];
    }


    public int getSize() {
        return size;
    }

    public void put(K key, V value) {
        if (key == null)
            return;

        int bucketIndex = getBucketIndex(hash(key));
        Node<K, V> newEntry = new Node<>(key, value);
        if (buckets[bucketIndex] == null) {
            buckets[bucketIndex] = newEntry;
            size++;
        } else {
            Node<K, V> previousNode = null;
            Node<K, V> currentNode = buckets[bucketIndex];
            while (currentNode != null) {
                if (currentNode.key.equals(key)) {
                    currentNode.value = value;
                    break;
                }
                previousNode = currentNode;
                currentNode = currentNode.next;
            }

            if (previousNode != null) {
                previousNode.next = newEntry;
                size++;
            }
        }

    }

    public boolean remove(K key) {
        int bucketIndex = getBucketIndex(hash(key));

        if (buckets[bucketIndex] != null) {
            Node<K, V> previous = null;
            Node<K, V> current = buckets[bucketIndex];

            while (current != null) {
                if (current.key.equals(key)) {
                    if (previous == null) {
                        buckets[bucketIndex] = buckets[bucketIndex].next;
                    } else {
                        previous.next = current.next;
                    }
                    size--;
                    return true;
                }
                previous = current;
                current = current.next;
            }
        }

        return false;
    }

    public V get(K key) {
        int bucketIndex = getBucketIndex(hash(key));
        if (buckets[bucketIndex] != null) {
            Node<K, V> node = buckets[bucketIndex];
            while (node != null) {
                if (node.key.equals(key))
                    return node.value;
                node = node.next;
            }
        }
        return null;
    }


    public int hash(K key) {
        return Objects.hashCode(key);
    }

    public int getBucketIndex(int hash) {
        return Math.abs(hash) % INITIAL_SIZE;
    }


    public void display() {

        for (int i = 0; i < INITIAL_SIZE; i++) {
            if (buckets[i] != null) {
                Node<K, V> node = buckets[i];
                while (node != null) {
                    System.out.println(node);
                    node = node.next;
                }
            }
        }

    }

}
