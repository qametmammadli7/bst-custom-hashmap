package com.qamet.hashmap;

import java.util.Objects;

public class Student {
    public long id;
    public String firstName;
    public String lastName;
    public String university;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Student(long id, String firstName, String lastName, String university) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.university = university;
    }
}
