package com.qamet.bst;

import java.util.Objects;

public class Student implements Comparable<Student> {
    public long id;

    public Student(long id, String name, int score) {
        this.id = id;
        this.name = name;
        this.score = score;
    }

    public String name;
    public int score;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


    @Override
    public int compareTo(Student o) {
        return Integer.compare(this.score, o.score);
    }

}
