package com.qamet.bst;

public class RedBlackTree {
    public Node root;//root node

    public RedBlackTree() {
        super();
        root = null;
    }

    static class Node {
        int data;
        Node left;
        Node right;
        char colour;
        Node parent;

        Node(int data) {
            super();
            this.data = data;
            this.left = null;
            this.right = null;
            this.colour = 'R';
            this.parent = null;
        }
    }

    Node rotateLeft(Node node) {
        Node x = node.right;
        Node y = x.left;
        x.left = node;
        node.right = y;
        node.parent = x;
        if (y != null)
            y.parent = node;
        return (x);
    }

    Node rotateRight(Node node) {
        Node x = node.left;
        Node y = x.right;
        x.right = node;
        node.left = y;
        node.parent = x;
        if (y != null)
            y.parent = node;
        return (x);
    }


    boolean ll = false;
    boolean rr = false;
    boolean lr = false;
    boolean rl = false;

    Node insert(Node root, int data) {
        boolean f = false;

        if (root == null)
            return (new Node(data));
        else if (data < root.data) {
            root.left = insert(root.left, data);
            root.left.parent = root;
            if (root != this.root) {
                if (root.colour == 'R' && root.left.colour == 'R')
                    f = true;
            }
        } else {
            root.right = insert(root.right, data);
            root.right.parent = root;
            if (root != this.root) {
                if (root.colour == 'R' && root.right.colour == 'R')
                    f = true;
            }
        }

        if (this.ll) {
            root = rotateLeft(root);
            root.colour = 'B';
            root.left.colour = 'R';
            this.ll = false;
        } else if (this.rr) {
            root = rotateRight(root);
            root.colour = 'B';
            root.right.colour = 'R';
            this.rr = false;
        } else if (this.rl) {
            root.right = rotateRight(root.right);
            root.right.parent = root;
            root = rotateLeft(root);
            root.colour = 'B';
            root.left.colour = 'R';

            this.rl = false;
        } else if (this.lr) {
            root.left = rotateLeft(root.left);
            root.left.parent = root;
            root = rotateRight(root);
            root.colour = 'B';
            root.right.colour = 'R';
            this.lr = false;
        }

        if (f) {
            if (root.parent.right == root) {
                if (root.parent.left == null || root.parent.left.colour == 'B') {
                    if (root.left != null && root.left.colour == 'R')
                        this.rl = true;
                    else if (root.right != null && root.right.colour == 'R')
                        this.ll = true;
                } else {
                    root.parent.left.colour = 'B';
                    root.colour = 'B';
                    if (root.parent != this.root)
                        root.parent.colour = 'R';
                }
            } else {
                if (root.parent.right == null || root.parent.right.colour == 'B') {
                    if (root.left != null && root.left.colour == 'R')
                        this.rr = true;
                    else if (root.right != null && root.right.colour == 'R')
                        this.lr = true;
                } else {
                    root.parent.right.colour = 'B';
                    root.colour = 'B';
                    if (root.parent != this.root)
                        root.parent.colour = 'R';
                }
            }
            f = false;
        }
        return (root);
    }

    public void insert(int data) {
        if (this.root == null) {
            this.root = new Node(data);
            this.root.colour = 'B';
        } else
            this.root = insert(this.root, data);
    }

    void inOrderTraversal(Node node) {
        if (node != null) {
            inOrderTraversal(node.left);
            System.out.printf("%d ", node.data);
            inOrderTraversal(node.right);
        }
    }

    public void inOrderTraversal() {
        inOrderTraversal(this.root);
    }

    void preOrderTraversal(Node node) {
        if (node != null) {
            System.out.printf("%d ", node.data);
            preOrderTraversal(node.left);
            preOrderTraversal(node.right);
        }
    }

    public void preOrderTraversal() {
        preOrderTraversal(this.root);
    }

    void postOrderTraversal(Node node) {
        if (node != null) {
            postOrderTraversal(node.left);
            postOrderTraversal(node.right);
            System.out.printf("%d ", node.data);
        }
    }

    public void postOrderTraversal() {
        postOrderTraversal(this.root);
    }

    void printTree(Node root, int space) {
        int i;
        if (root != null) {
            space = space + 10;
            printTree(root.right, space);
            System.out.print("\n");
            for (i = 10; i < space; i++) {
                System.out.print(" ");
            }
            System.out.printf("%d", root.data);
            System.out.print("\n");
            printTree(root.left, space);
        }
    }

    public void printTree() {
        printTree(this.root, 0);
    }

    public static void main(String[] args) {
        RedBlackTree tree = new RedBlackTree();
        tree.insert(1);
        tree.insert(5);
        tree.insert(7);
        tree.insert(4);
        tree.insert(6);
        tree.insert(8);
        tree.insert(9);
        tree.insert(3);
        tree.insert(10);

        tree.printTree();
        tree.inOrderTraversal();
        System.out.println();
        tree.preOrderTraversal();
        System.out.println();
        tree.postOrderTraversal();
    }

}

